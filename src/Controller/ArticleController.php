<?php
/**
 * Created by PhpStorm.
 * User: Jakub Slapnička
 * Date: 08.02.2019
 * Time: 9:43
 */

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */

    public function homepage()
    {
        //Pripojeni k databazi
        $link = mysqli_connect("localhost", "root", "MyNewPass", "blog_symfony_db");

        //Pokud pripejeni k databazi selze
        if(mysqli_connect_error()) {
            die("Database connection error");
        }

        $articles = [];
        $query = "SELECT * FROM `articles`";
        $result = mysqli_query($link, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            array_push($articles, $row);
        }



        return $this->render('article/homepage.html.twig', [
            'title' => "Seznam clanku",
            'articles' => $articles,
        ]);

    }

    /**
     * @Route("/contact", name="contact_form")
     */
    public function contact()
    {
        return $this->render('article/contact.html.twig', [
            'title' => "Kontakt",
        ]);
    }
}